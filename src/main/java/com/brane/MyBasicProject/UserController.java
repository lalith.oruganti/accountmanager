package com.brane.MyBasicProject;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {
	@GetMapping("/")
	void call() {
		System.out.println("called");
	}
	@GetMapping("/{id}")
	void called(@PathVariable(value="id") int id) {
		System.out.println("called"+id);
	}
	@PostMapping("/")
	private void calling(@RequestBody User user) {
		System.out.println("got user name---"+user.getName());
	}
	@PutMapping("/{id}")
	private void update(@PathVariable(value="id") int userid) {
		System.out.println(userid);
		
	}
}