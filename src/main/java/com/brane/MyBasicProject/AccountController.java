package com.brane.MyBasicProject;

import org.springframework.web.bind.annotation.*;

@RequestMapping("account")
public class AccountController {
	
	@GetMapping("/")
	public void fetchAllAccounts() {
		System.out.println("All the accounts are fetched");
	}
	@GetMapping("/{id}")
	public void fetchAccountById(@PathVariable(value = "id") int accountid) {
		System.out.println("Account with id" +accountid);
	}
	@PostMapping("/")
	public void createAccount(@RequestBody Account acc) {
		System.out.println("Account holder name"+acc.getName());
	}
}
