package com.brane.MyBasicProject;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class MyBasicProjectApplication {

	public static void main(String[] args) {
		SpringApplication.run(MyBasicProjectApplication.class, args);
	}

}
